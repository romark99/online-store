package com.brest.bstu.po1.pip.model;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Model class "Goods".
 */
public class Goods {

    private static final String NAME_WRONG_SIZE =
            "The name of goods must be between 2 and 255 characters.";
    private static final String QUANTITY_IS_NEGATIVE =
            "The quantity of goods must be non-negative.";
    private static final String QUANTITY_IS_TOO_BIG =
            "The quantity of goods must be less than 100000.";
    private static final String PRICE_IS_NOT_POSITIVE =
            "The price of goods must be positive.";
    private static final String DESCRIPTION_IS_TOO_BIG =
            "The description of goods must not exceed 1000 characters.";
    private static final String ADDING_DATE_IS_FUTURE =
            "The adding date must not be future.";

    /**
     * The goods' id.
     */
    private Integer id;

    /**
     * The goods' name.
     */
    @Size(min = 2, max = 255, message = NAME_WRONG_SIZE)
    private String name;

    /**
     * The goods' quantity at the store.
     */
    @Min(value = 0, message = QUANTITY_IS_NEGATIVE)
    @Max(value = 100000, message = QUANTITY_IS_TOO_BIG)
    private Integer quantity;

    /**
     * The image name.
     */
    private String imageExtension;

    /**
     * The goods' image.
     */
    private String image;

    /**
     * The goods' description.
     */
    @Size(max = 1000, message = DESCRIPTION_IS_TOO_BIG)
    private String description;

    /**
     * The goods' price.
     */
    @DecimalMin(value = "0.01", message = PRICE_IS_NOT_POSITIVE)
    private BigDecimal price;

    /**
     * Adding date.
     */
    @PastOrPresent(message = ADDING_DATE_IS_FUTURE)
    private Date addingDate;

    public Goods() {
    }

    public Goods(@Size(min = 2, max = 255, message = NAME_WRONG_SIZE) String name, @Min(value = 0, message = QUANTITY_IS_NEGATIVE) @Max(value = 100000, message = QUANTITY_IS_TOO_BIG) Integer quantity, String imageName, String image, @Size(max = 1000, message = DESCRIPTION_IS_TOO_BIG) String description, @DecimalMin(value = "0.01", message = PRICE_IS_NOT_POSITIVE) BigDecimal price, @PastOrPresent(message = ADDING_DATE_IS_FUTURE) Date addingDate) {
        this.name = name;
        this.quantity = quantity;
        this.imageExtension = imageName;
        this.image = image;
        this.description = description;
        this.price = price;
        this.addingDate = addingDate;
    }

    /* Getters and setters */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getImageExtension() {
        return imageExtension;
    }

    public void setImageExtension(String imageName) {
        this.imageExtension = imageName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getAddingDate() {
        return addingDate;
    }

    public void setAddingDate(Date addingDate) {
        this.addingDate = addingDate;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", imageName='" + imageExtension + '\'' +
                ", image=" + image +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", addingDate=" + addingDate +
                '}';
    }
}
