package com.brest.bstu.po1.pip.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Model class 'User'.
 */
public class User {

    private static final String SURNAME_WRONG_SIZE =
            "The surname of user must be between 1 and 100 characters.";
    private static final String NAME_WRONG_SIZE =
            "The name of user must be between 1 and 100 characters.";
    private static final String LOGIN_IS_EMPTY =
            "The login of user must not be empty.";
    private static final String LOGIN_WRONG_SIZE =
            "The login of user must be 3 and 50 characters.";
    private static final String PASSWORD_WRONG_SIZE =
            "The password of user must be between 3 and 50 characters.";
    private static final String EMAIL_WRONG_SIZE =
            "The login of user must be between 5 and 100 characters.";
    private static final String EMAIL_WRONG_FORMAT =
            "The email of user does not match the email format.";

    /**
     * The user's id.
     */
    private Integer id;

    /**
     * The user's surname.
     */
    @Size(min = 1, max = 100, message = SURNAME_WRONG_SIZE)
    private String surname;

    /**
     * The user's name.
     */
    @Size(min = 1, max = 100, message = NAME_WRONG_SIZE)
    private String name;

    /**
     * The user's login.
     */
    @NotBlank(message = LOGIN_IS_EMPTY)
    @Size(min = 3, max = 50, message = LOGIN_WRONG_SIZE)
    private String login;

    /**
     * The user's password.
     */
    @Size(min = 3, max = 50, message = PASSWORD_WRONG_SIZE)
    private String password;

    /**
     * The confirmed password.
     */
    private transient String confirmedPassword;

    /**
     * The user's email.
     */
    @Size(min = 5, max = 100, message = EMAIL_WRONG_SIZE)
    @Email(message = EMAIL_WRONG_FORMAT)
    private String email;

    /**
     * If the user is an admin.
     */
    private Boolean isAdmin;

    public User() {
    }

    public User(String surname, String name, String login, String password, String email, Boolean isAdmin) {
        this.surname = surname;
        this.name = name;
        this.login = login;
        this.password = password;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    /* Getters and Setters */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    @Override
    public String toString() {
        return "User{"
                + "id=" + id
                + ", surname='" + surname + '\''
                + ", name='" + name + '\''
                + ", login='" + login + '\''
                + ", password='" + password + '\''
                + ", email='" + email + '\''
                + ", isAdmin=" + isAdmin
                + '}';
    }
}
