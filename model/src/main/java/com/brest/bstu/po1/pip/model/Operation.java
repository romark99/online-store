package com.brest.bstu.po1.pip.model;

public class Operation {

    private Integer id;

    private Integer userId;

    private Integer goodsId;

    private Integer quantity;

    private Boolean isHistory;

    public Operation() {
    }

    public Operation(Integer id, Integer userId, Integer goodsId, Integer quantity, Boolean isHistory) {
        this.id = id;
        this.userId = userId;
        this.goodsId = goodsId;
        this.quantity = quantity;
        this.isHistory = isHistory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", userId=" + userId +
                ", goodsId=" + goodsId +
                ", quantity=" + quantity +
                ", isHistory=" + isHistory +
                '}';
    }
}
