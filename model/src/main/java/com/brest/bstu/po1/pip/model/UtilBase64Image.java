package com.brest.bstu.po1.pip.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Base64;

public class UtilBase64Image {

    private static final Logger LOGGER = LogManager.getLogger();

    public static String encoder(MultipartFile imageFile) {
        LOGGER.debug("encoder({})", imageFile);
        byte[] imageData = new byte[0];
        try {
            imageData = imageFile.getBytes();
        } catch (IOException e) {
            LOGGER.error("IOException @_@");
        }
        String base64Image = Base64.getEncoder().encodeToString(imageData);
        return base64Image;
    }

    public static String encoder(String imagePath, String extension) {
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            LOGGER.debug("Image found " + imagePath);
            // Reading a Image file from file system
            String base64Image = "";
            byte[] imageData = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
            return "data:image/" + extension + ";base64," + base64Image;
        } catch (FileNotFoundException e) {
            LOGGER.debug("Image not found " + e);
            return null;
        } catch (IOException ioe) {
            LOGGER.error("Exception while reading the Image " + ioe);
        }
        return null;
    }

    public static void decoder(String base64Image, String pathFile) {
        try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
            imageOutFile.write(imageByteArray);
        } catch (FileNotFoundException e) {
            LOGGER.error("Image not found" + e);
        } catch (IOException ioe) {
            LOGGER.error("Exception while reading the Image " + ioe);
        }
    }
}