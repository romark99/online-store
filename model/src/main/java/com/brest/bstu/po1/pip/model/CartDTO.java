package com.brest.bstu.po1.pip.model;

import java.math.BigDecimal;

public class CartDTO {

    private Integer goodsId;

    private String goodsName;

    private Integer quantity;

    private BigDecimal goodsPrice;

    private BigDecimal goodsCost;

    private String image;

    private String imageExtension;

    private String date;


    public CartDTO() {
    }

    public CartDTO(Integer goodsId, String goodsName, Integer quantity, BigDecimal goodsPrice, BigDecimal goodsCost, String image, String imageExtension, String date) {
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.quantity = quantity;
        this.goodsPrice = goodsPrice;
        this.goodsCost = goodsCost;
        this.image = image;
        this.imageExtension = imageExtension;
        this.date = date;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public BigDecimal getGoodsCost() {
        return goodsCost;
    }

    public void setGoodsCost(BigDecimal goodsCost) {
        this.goodsCost = goodsCost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CartDTO{" +
                "goodsId=" + goodsId +
                ", goodsName='" + goodsName + '\'' +
                ", quantity=" + quantity +
                ", goodsPrice=" + goodsPrice +
                ", goodsCost=" + goodsCost +
                ", date='" + date + '\'' +
                '}';
    }

    public String getImageExtension() {
        return imageExtension;
    }

    public void setImageExtension(String imageExtension) {
        this.imageExtension = imageExtension;
    }
}
