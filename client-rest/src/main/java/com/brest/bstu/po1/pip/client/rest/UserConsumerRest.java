package com.brest.bstu.po1.pip.client.rest;

import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

/**
 * Implementation of UserService. Gets data from rest-app.
 */
public class UserConsumerRest implements UserService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private String url;

    private RestTemplate restTemplate;

    @Autowired
    public UserConsumerRest(String url, RestTemplate restTemplate) {
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<User> getAllUsers() {
        LOGGER.debug("Started: getUsers()");
        ResponseEntity responseEntity =
                restTemplate.getForEntity(url, Collection.class);
        Collection<User> users = (Collection<User>) responseEntity.getBody();
        LOGGER.debug("Finished: getUsers() -> {}", users);
        return users;
    }

    @Override
    public User getUserById(Integer userId) {
        LOGGER.debug("Started: getUserById({})", userId);
        ResponseEntity<User> responseEntity =
                restTemplate.getForEntity(url + "/get/" + userId, User.class);
        User user = responseEntity.getBody();
        LOGGER.debug("Finished: getUserById() -> {}", user);
        return user;
    }

    @Override
    public User addUser(User user) {
        LOGGER.debug("Started: addUser({})", user);
        ResponseEntity<User> responseEntity =
                restTemplate.postForEntity(url, user, User.class);
        User newUser = responseEntity.getBody();
        LOGGER.debug("Finished: addUser() -> {}", newUser);
        return newUser;
    }

    @Override
    public void updateUser(User user) {
        LOGGER.debug("Started: updateUser({})", user);
        restTemplate.postForEntity(
                url + "/" + user.getId(), user, void.class);
        LOGGER.debug("Finished: updateUser()");
    }

    @Override
    public void deleteUserById(Integer userId) {
        LOGGER.debug("Started: deleteUserById({})", userId);
        restTemplate.delete(
                url + "/" + userId);
        LOGGER.debug("Finished: deleteUserById()");
    }

    @Override
    public User findUserByLogin(String login) {
        LOGGER.debug("Started: findUserByLogin({})", login);
        ResponseEntity<User> responseEntity =
                restTemplate.getForEntity(url + "/login/" + login, User.class);
        User user = responseEntity.getBody();
        LOGGER.debug("Finished: findUserByLogin() -> {}", user);
        return user;
    }
}
