package com.brest.bstu.po1.pip.client.rest;

import com.brest.bstu.po1.pip.model.Goods;
import com.brest.bstu.po1.pip.service.GoodsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class GoodsConsumerRest implements GoodsService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private String url;

    private RestTemplate restTemplate;

    @Autowired
    public GoodsConsumerRest(String url, RestTemplate restTemplate) {
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Goods> getAllGoods() {
        LOGGER.debug("Started: getAllGoods()");
        ResponseEntity responseEntity =
                restTemplate.getForEntity(url, Collection.class);
        Collection<Goods> goodsList = (Collection<Goods>) responseEntity.getBody();
        LOGGER.debug("Finished: getAllGoods() -> {}", goodsList);
        return goodsList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Goods> getThreeGoods() {
        LOGGER.debug("Started: getThreeGoods()");
        ResponseEntity responseEntity =
                restTemplate.getForEntity(url + "/three", Collection.class);
        Collection<Goods> goodsList = (Collection<Goods>) responseEntity.getBody();
        LOGGER.debug("Finished: getThreeGoods() -> {}", goodsList);
        return goodsList;
    }

    @Override
    public Goods getGoodsById(Integer goodsId) {
        LOGGER.debug("Started: getGoodsById({})", goodsId);
        ResponseEntity<Goods> responseEntity =
                restTemplate.getForEntity(url + "/get/" + goodsId, Goods.class);
        Goods goods = responseEntity.getBody();
        LOGGER.debug("Finished: getGoodsById() -> {}", goods);
        return goods;
    }

    @Override
    public Goods addGoods(Goods goods) {
        LOGGER.debug("Started: addGoods({})", goods);

        ResponseEntity<Goods> responseEntity =
                restTemplate.postForEntity(url, goods, Goods.class);

        Goods newGoods = responseEntity.getBody();
        LOGGER.debug("Finished: addGoods() -> {}", newGoods);
        return newGoods;
    }

    @Override
    public void updateGoods(Goods goods) {
        LOGGER.debug("Started: updateGoods({})", goods);
        restTemplate.postForEntity(
                url + "/" + goods.getId(), goods, void.class);
        LOGGER.debug("Finished: updateGoods()");
    }

    @Override
    public void deleteGoodsById(Integer goodsId) {
        LOGGER.debug("Started: deleteGoodsById({})", goodsId);
        restTemplate.delete(
                url + "/" + goodsId);
        LOGGER.debug("Finished: deleteGoodsById()");
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Goods> searchGoods(String substring) {
        LOGGER.debug("Started: searchGoods({})", substring);
        ResponseEntity responseEntity =
                restTemplate.getForEntity(url + "/search/" + substring, Collection.class);
        Collection<Goods> goodsList = (Collection<Goods>) responseEntity.getBody();
        LOGGER.debug("Finished: searchGoods() -> {}", goodsList);
        return goodsList;
    }

}
