package com.brest.bstu.po1.pip.client;

public class ServerDataAccessException extends RuntimeException {

    public ServerDataAccessException(String message) {
        super(message);
    }
}
