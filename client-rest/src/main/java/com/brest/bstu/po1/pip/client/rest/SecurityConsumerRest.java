package com.brest.bstu.po1.pip.client.rest;

import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.service.SecurityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class SecurityConsumerRest implements SecurityService {

    private static final Logger LOGGER = LogManager.getLogger();

    private String url;

    private RestTemplate restTemplate;

    @Autowired
    public SecurityConsumerRest(String url, RestTemplate restTemplate) {
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public void autoLogin(String login, String confirmedPassword) {
        LOGGER.debug("Started: autologin({}, {})", login, confirmedPassword);
        User user = new User();
        user.setLogin(login);
        user.setConfirmedPassword(confirmedPassword);
        restTemplate.postForEntity(
                url + "/autologin", user, void.class);
        LOGGER.debug("Finished: autologin()");
    }
}
