package com.brest.bstu.po1.pip.client.rest;

import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;
import com.brest.bstu.po1.pip.service.OperationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collection;

public class OperationConsumerRest implements OperationService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private String url;

    private RestTemplate restTemplate;

    @Autowired
    public OperationConsumerRest(String url, RestTemplate restTemplate) {

        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public void updateCart(Operation operation) {
        LOGGER.debug("Started: updateCart({})", operation);
        restTemplate.postForEntity(url + "/cart/update", operation, void.class);
        LOGGER.debug("Finished: updateCart()");
    }

    @Override
    public Operation getOperationIfExists(Operation operation) {
        LOGGER.debug("Started: getOperationIfExists({})", operation);
        ResponseEntity<Operation> responseEntity =
                restTemplate.postForEntity(url, operation, Operation.class);
        Operation newOperation = responseEntity.getBody();
        LOGGER.debug("Finished: getOperationIfExists() -> {}", newOperation);
        return newOperation;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<CartDTO> getCartDTOs(Integer userId, Boolean isHistory) {
        LOGGER.debug("Started: getCartDTOs({}, {})", userId, isHistory);
        ResponseEntity responseEntity =
                restTemplate.getForEntity(url + "/carts/get/" + userId + "/" + isHistory, Collection.class);
        Collection<CartDTO> carts = (Collection<CartDTO>) responseEntity.getBody();
        LOGGER.debug("Finished: getCartDTOs() -> {}", carts);
        return carts;
    }

    @Override
    public BigDecimal getCartTotal(Integer userId, Boolean isHistory) {
        LOGGER.debug("Started: getCartTotal({}, {})", userId, isHistory);
        ResponseEntity<BigDecimal> responseEntity =
                restTemplate.getForEntity(url + "/carts/total/" + userId + "/" + isHistory, BigDecimal.class);
        BigDecimal total = responseEntity.getBody();
        LOGGER.debug("Finished: getCartTotal() -> {}", total);
        return total;
    }

    @Override
    public void deleteFromCart(Integer userId, Integer goodsId) {
        LOGGER.debug("Started: deleteUserById({}, {})", userId, goodsId);
        restTemplate.delete(
                url + "/carts/" + userId + "/" + goodsId);
        LOGGER.debug("Finished: deleteUserById()");
    }

    @Override
    public void pay(Integer userId) {
        LOGGER.debug("Started: pay({}, {})", userId);
        restTemplate.getForEntity(
                url + "/pay/" + userId, void.class);
        LOGGER.debug("Finished: pay()");
    }
}
