package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-db-spring.xml", "classpath:test-dao.xml", "classpath:dao.xml"})
@Rollback
@Transactional
public class UserDaoImplTest {

    @Autowired
    UserDao userDao;

    @Test
    public void getAllUsers() {
        Collection<User> users = userDao.getAllUsers();
        Assert.assertFalse(users.isEmpty());
    }

    @Test
    public void getUserById() {
        User user = userDao.getUserById(1);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, (int) user.getId());
        Assert.assertEquals("Марковский", user.getSurname());
    }

    @Test
    public void addUser() {
        Collection<User> users = userDao.getAllUsers();
        int size = users.size();

        User user = new User("Петров", "Петр", "petr", "petr", null, false);

        User returnedUser = userDao.addUser(user);
        Integer id = returnedUser.getId();
        Assert.assertNotNull(id);
        User addedUser = userDao.getUserById(id);

        Assert.assertEquals("Петров", addedUser.getSurname());
        Assert.assertEquals("Петр", addedUser.getName());
        Assert.assertEquals("petr", addedUser.getLogin());
        Assert.assertEquals("petr", addedUser.getPassword());
        Assert.assertEquals(false, addedUser.getIsAdmin());

        Assert.assertEquals(size + 1, userDao.getAllUsers().size());
    }

    @Test
    public void updateUser() {
        User user = new User("Павлов", "Иван", "pavel", "pavel", null, false);

        User addedUser = userDao.addUser(user);
        user.setName("Павел");
        user.setLogin("pasha");
        user.setPassword("pasha");

        userDao.updateUser(addedUser);
        User updatedUser = userDao.getUserById(addedUser.getId());

        Assert.assertEquals(addedUser.getId(), updatedUser.getId());
        Assert.assertEquals("Павлов", addedUser.getSurname());
        Assert.assertEquals("Павел", addedUser.getName());
        Assert.assertEquals("pasha", addedUser.getLogin());
        Assert.assertEquals("pasha", addedUser.getPassword());
        Assert.assertEquals(false, addedUser.getIsAdmin());
    }

    @Test
    public void deleteUserById () {
        User user = new User("Павлов", "Иван", "pavel", "pavel", null, false);

        User addedUser = userDao.addUser(user);
        Collection<User> users = userDao.getAllUsers();
        int oldSize = users.size();

        userDao.deleteUserById(addedUser.getId());

        users = userDao.getAllUsers();
        int newSize = users.size();

        Assert.assertEquals(oldSize - 1, newSize);
    }
}