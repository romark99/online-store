package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.Goods;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-db-spring.xml", "classpath:test-dao.xml", "classpath:dao.xml"})
@Rollback
@Transactional
public class GoodsDaoImplTest {

    @Autowired
    GoodsDao goodsDao;

    @Test
    public void getAllGoods() {
        Collection<Goods> goods = goodsDao.getAllGoods();
        Assert.assertFalse(goods.isEmpty());
    }

    @Test
    public void getThreeGoods() {
        Collection<Goods> goods = goodsDao.getThreeGoods();
        int size = goods.size();
        Assert.assertTrue(size <= 3);
    }

    @Test
    public void getGoodsById() {
        Goods goods = goodsDao.getGoodsById(1);
        Assert.assertNotNull(goods);
        Assert.assertEquals(1, (int) goods.getId());
        Assert.assertEquals("Шоколад", goods.getName());
    }

    @Test
    public void addGoods() {
        Collection<Goods> goods = goodsDao.getAllGoods();
        int size = goods.size();

        Goods one_goods = new Goods("Мороженое", 2, null, null, "Очень вкусное мороженое",  new BigDecimal(60.40), Date.valueOf("2018-01-01"));

        Goods returnedGoods = goodsDao.addGoods(one_goods);
        Integer id = returnedGoods.getId();
        Assert.assertNotNull(id);
        Goods addedGoods = goodsDao.getGoodsById(id);

        Assert.assertEquals("Мороженое",addedGoods.getName());
        Assert.assertEquals(2, (int) addedGoods.getQuantity());
        Assert.assertEquals(60.40, addedGoods.getPrice().doubleValue(), 0.00000001);
        Assert.assertEquals("Очень вкусное мороженое", addedGoods.getDescription());

        Assert.assertEquals(size + 1, goodsDao.getAllGoods().size());
    }

    @Test
    public void updateGoods() {
        Goods goods = new Goods("Печенье", 7, null, null, "Обычное печенье.",  new BigDecimal(7.15), Date.valueOf("2018-01-02"));

        Goods addedGoods = goodsDao.addGoods(goods);
        addedGoods.setQuantity(5);
        addedGoods.setPrice(new BigDecimal(8.15));
        addedGoods.setDescription("Необычное печенье.");

        goodsDao.updateGoods(addedGoods);
        Goods updatedGoods = goodsDao.getGoodsById(addedGoods.getId());

        Assert.assertEquals(addedGoods.getId(), updatedGoods.getId());
        Assert.assertEquals("Печенье", updatedGoods.getName());
        Assert.assertEquals(5, (int) updatedGoods.getQuantity());
        Assert.assertEquals(8.15, updatedGoods.getPrice().doubleValue(), 0.00000001);
        Assert.assertEquals("Необычное печенье.", addedGoods.getDescription());
    }

    @Test
    public void deleteGoodsById () {
        Goods goods = new Goods("Печенье", 7, null, null, "Обычное печенье.",  new BigDecimal(7.15), Date.valueOf("2017-03-03"));

        Goods addedGoods = goodsDao.addGoods(goods);

        Collection<Goods> listGoods = goodsDao.getAllGoods();
        int oldSize = listGoods.size();

        goodsDao.deleteGoodsById(addedGoods.getId());

        listGoods = goodsDao.getAllGoods();
        int newSize = listGoods.size();

        Assert.assertEquals(oldSize - 1, newSize);
    }

    @Test
    public void searchGoods() {
        String substring = "еты";
        Collection<Goods> goods = goodsDao.searchGoods(substring);
        int size = goods.size();
        Assert.assertEquals(2, size);
        for (Goods good: goods) {
            Assert.assertTrue(good.getName().contains(substring));
        }
    }
}