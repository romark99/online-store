package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Objects;

public class OperationDaoImpl implements OperationDao {

    private static final Logger LOGGER = LogManager.getLogger();

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${operation.checkIfAdded}")
    private String CHECK_IF_ADDED;

    @Value("${operation.increaseCartQuantity}")
    private String INCREASE_CART_QUANTITY;

    @Value("${operation.decreaseCartQuantity}")
    private String DECREASE_CART_QUANTITY;

    @Value("${operation.add}")
    private String ADD_OPERATION;

    @Value("${operation.select}")
    private String SELECT_OPERATION;

    @Value("${operation.delete}")
    private String DELETE_OPERATION;

    @Value("${operation.getCartDTOs}")
    private String GET_CART_DTOS;

    @Value("${operation.getCartTotal}")
    private String GET_CART_TOTAL;

    @Value("${operation.pay}")
    private String PAY;

    //constant fields

    private static final String USER_ID = "user_id";

    private static final String GOODS_ID = "goods_id";

    private static final String GOODS_NAME = "goods_name";

    private static final String QUANTITY = "quantity";

    private static final String GOODS_PRICE = "goods_price";

    private static final String GOODS_COST = "goods_cost";

    private static final String IS_HISTORY = "is_history";

    private static final String IMAGE = "image";

    private static final String IMAGE_NAME = "image_name";

    private static final String DATE = "date";

    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public boolean checkIfAlreadyAdded(Operation operation) {
        LOGGER.debug("checkIfAlreadyAdded({})", operation);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(GOODS_ID, operation.getGoodsId());
        namedParameters.addValue(USER_ID, operation.getUserId());
        namedParameters.addValue(IS_HISTORY, operation.getIsHistory());
        Integer quantity = namedParameterJdbcTemplate.queryForObject(CHECK_IF_ADDED, namedParameters, Integer.class);
        assert quantity != null;
        return !quantity.equals(0);
    }

    @Override
    public void addOperation(Operation operation) {
        LOGGER.debug("addOperation({})", operation);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(GOODS_ID, operation.getGoodsId());
        namedParameters.addValue(USER_ID, operation.getUserId());
        namedParameters.addValue(QUANTITY, operation.getQuantity());
        namedParameters.addValue(IS_HISTORY, operation.getIsHistory());

        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(ADD_OPERATION, namedParameters, generatedKeyHolder, new String[] {"id"});
        operation.setId(Objects.requireNonNull(generatedKeyHolder.getKey()).intValue());
    }

    @Override
    public void changeCartQuantity(Operation operation, Boolean isIncrease) {
        LOGGER.debug("changeCartQuantity({})", operation, isIncrease);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(GOODS_ID, operation.getGoodsId());
        namedParameters.addValue(USER_ID, operation.getUserId());
        namedParameters.addValue(IS_HISTORY, operation.getIsHistory());
        namedParameters.addValue(QUANTITY, operation.getQuantity());
        namedParameterJdbcTemplate.update(isIncrease ? INCREASE_CART_QUANTITY : DECREASE_CART_QUANTITY, namedParameters);
    }

    @Override
    public Operation selectExistingOperation(Integer userId, Integer goodsId, Boolean isHistory) {
        LOGGER.debug("selectExistingOperation({}, {}, {})", userId, goodsId, isHistory);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(GOODS_ID, goodsId);
        namedParameters.addValue(USER_ID, userId);
        namedParameters.addValue(IS_HISTORY, isHistory);
        Operation operation = namedParameterJdbcTemplate.queryForObject(SELECT_OPERATION,
                namedParameters, BeanPropertyRowMapper.newInstance(Operation.class));
        return operation;
    }

    @Override
    public Collection<CartDTO> getCartDTOs(Integer userId, Boolean isHistory) {
        LOGGER.debug("getCartDTOs ({}, {})", userId, isHistory);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(USER_ID, userId);
        namedParameters.addValue(IS_HISTORY, isHistory);
        Collection<CartDTO> cartDTOs =
                namedParameterJdbcTemplate.query(GET_CART_DTOS, namedParameters, new CartDTORowMapper());
        return cartDTOs;
    }

    @Override
    public BigDecimal getCartTotal(Integer userId, Boolean isHistory) {
        LOGGER.debug("getCartTotal({}, {})", userId, isHistory);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(USER_ID, userId);
        namedParameters.addValue(IS_HISTORY, isHistory);
        BigDecimal total =
                namedParameterJdbcTemplate.queryForObject(GET_CART_TOTAL, namedParameters, BigDecimal.class);
        return total;
    }

    @Override
    public void deleteOperation(Operation oldOperation) {
        LOGGER.debug("deleteOperation({})", oldOperation);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(GOODS_ID, oldOperation.getGoodsId());
        namedParameters.addValue(USER_ID, oldOperation.getUserId());
        namedParameters.addValue(IS_HISTORY, oldOperation.getIsHistory());
        namedParameterJdbcTemplate.update(DELETE_OPERATION, namedParameters);
    }

    @Override
    public void pay(Integer userId) {
        LOGGER.debug("pay({})", userId);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(PAY, namedParameters);
    }

    private class CartDTORowMapper implements RowMapper<CartDTO> {

        @Override
        public CartDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            CartDTO cart = new CartDTO();
            cart.setGoodsId(resultSet.getInt(GOODS_ID));
            cart.setGoodsName(resultSet.getString(GOODS_NAME));
            cart.setQuantity(resultSet.getInt(QUANTITY));
            cart.setGoodsPrice(resultSet.getBigDecimal(GOODS_PRICE));
            cart.setGoodsCost(resultSet.getBigDecimal(GOODS_COST));
            cart.setImage(resultSet.getString(IMAGE));
            cart.setImageExtension(resultSet.getString(IMAGE_NAME));
            cart.setDate(resultSet.getString(DATE));
            return cart;
        }
    }
}
