package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.Goods;
import com.brest.bstu.po1.pip.model.Operation;

import java.util.Collection;

public interface GoodsDao {

    Collection<Goods> getAllGoods();

    Collection<Goods> getThreeGoods();

    Goods getGoodsById(Integer goodsId);

    Goods addGoods(Goods goods);

    void updateGoods(Goods goods);

    void deleteGoodsById(Integer goodsId);

    Collection<Goods> searchGoods(String substring);

    void changeQuantity(Operation operation, Boolean isIncrease);
}
