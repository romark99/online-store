package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.Goods;
import com.brest.bstu.po1.pip.model.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Objects;


public class GoodsDaoImpl implements GoodsDao {

    private static final Logger LOGGER = LogManager.getLogger();

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${goods.select}")
    private String GET_ALL_GOODS_SQL;

    @Value("${goods.select3}")
    private String GET_THREE_GOODS_SQL;

    @Value("${goods.selectById}")
    private String GET_GOODS_BY_ID_SQL;

    @Value("${goods.insert}")
    private String ADD_GOODS_SQL;

    @Value("${goods.update}")
    private String UPDATE_GOODS_SQL;

    @Value("${goods.delete}")
    private String DELETE_GOODS_SQL;

    @Value("${goods.search}")
    private String SEARCH_GOODS_SQL;

    @Value("${goods.increaseQuantity}")
    private String INCREASE_QUANTITY;

    @Value("${goods.decreaseQuantity}")
    private String DECREASE_QUANTITY;

    //constant fields

    private static final String ID = "id";

    private static final String NAME = "name";

    private static final String QUANTITY = "quantity";

    private static final String IMAGE_NAME = "image_name";

    private static final String IMAGE = "image";

    private static final String DESCRIPTION = "description";

    private static final String PRICE = "price";

    private static final String ADDING_DATE = "adding_date";

    private static final String SUBSTRING = "substring";

    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Collection<Goods> getAllGoods() {
        LOGGER.debug("getAllGoods()");
        Collection<Goods> goods = namedParameterJdbcTemplate.query(GET_ALL_GOODS_SQL, new GoodsRowMapper());
        return goods;
    }

    @Override
    public Collection<Goods> getThreeGoods() {
        LOGGER.debug("getThreeGoods()");
        Collection<Goods> goods = namedParameterJdbcTemplate.query(GET_THREE_GOODS_SQL, new GoodsRowMapper());
        return goods;
    }

    @Override
    public Goods getGoodsById(Integer goodsId) {
        LOGGER.debug("getGoodsById({})", goodsId);
        SqlParameterSource namedParameters =
                new MapSqlParameterSource(ID, goodsId);
        Goods goods = namedParameterJdbcTemplate.queryForObject(GET_GOODS_BY_ID_SQL,
                namedParameters, BeanPropertyRowMapper.newInstance(Goods.class));
        return goods;
    }

    @Override
    public Goods addGoods(Goods goods) {
        LOGGER.debug("addGoods({})", goods);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(NAME, goods.getName());
        namedParameters.addValue(QUANTITY, goods.getQuantity());
        namedParameters.addValue(IMAGE_NAME, goods.getImageExtension());
        namedParameters.addValue(IMAGE, goods.getImage());
        namedParameters.addValue(DESCRIPTION, goods.getDescription());
        namedParameters.addValue(PRICE, goods.getPrice());

        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(ADD_GOODS_SQL, namedParameters, generatedKeyHolder, new String[] {"id"});
        goods.setId(Objects.requireNonNull(generatedKeyHolder.getKey()).intValue());
        return goods;
    }

    @Override
    public void updateGoods(Goods goods) {
        LOGGER.debug("updateGoods({})", goods);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(ID, goods.getId());
        namedParameters.addValue(NAME, goods.getName());
        namedParameters.addValue(QUANTITY, goods.getQuantity());
        namedParameters.addValue(IMAGE_NAME, goods.getImageExtension());
        namedParameters.addValue(IMAGE, goods.getImage());
        namedParameters.addValue(DESCRIPTION, goods.getDescription());
        namedParameters.addValue(PRICE, goods.getPrice());
        namedParameters.addValue(ADDING_DATE, goods.getAddingDate());

        namedParameterJdbcTemplate.update(UPDATE_GOODS_SQL, namedParameters);
    }

    @Override
    public void deleteGoodsById(Integer goodsId) {
        LOGGER.debug("deleteGoodsById({})", goodsId);
        SqlParameterSource namedParameters = new MapSqlParameterSource(ID, goodsId);
        namedParameterJdbcTemplate.update(DELETE_GOODS_SQL, namedParameters);
    }

    @Override
    public Collection<Goods> searchGoods(String substring) {
        LOGGER.debug("searchGoods({})", substring);
        SqlParameterSource namedParameters =
                new MapSqlParameterSource(SUBSTRING, "%" + substring + "%");
        Collection<Goods> goodsList =
                namedParameterJdbcTemplate.query(SEARCH_GOODS_SQL, namedParameters, new GoodsRowMapper());
        return goodsList;
    }

    @Override
    public void changeQuantity(Operation operation, Boolean isIncrease) {
        LOGGER.debug("changeQuantity({})", operation, isIncrease);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(ID, operation.getGoodsId());
        namedParameters.addValue(QUANTITY, operation.getQuantity());
        namedParameterJdbcTemplate.update(isIncrease ? INCREASE_QUANTITY : DECREASE_QUANTITY, namedParameters);
    }

    private class GoodsRowMapper implements RowMapper<Goods> {

        @Override
        public Goods mapRow(ResultSet resultSet, int i) throws SQLException {
            Goods goods = new Goods();
            goods.setId(resultSet.getInt(ID));
            goods.setName(resultSet.getString(NAME));
            goods.setQuantity(resultSet.getInt(QUANTITY));
            goods.setImageExtension(resultSet.getString(IMAGE_NAME));
            goods.setImage(resultSet.getString(IMAGE));
            goods.setDescription(resultSet.getString(DESCRIPTION));
            goods.setPrice(resultSet.getBigDecimal(PRICE));
            goods.setAddingDate(resultSet.getDate(ADDING_DATE));
            return goods;
        }
    }
}
