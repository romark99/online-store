package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.User;

import java.util.Collection;

public interface UserDao {
    Collection<User> getAllUsers();

    User getUserById(Integer userId);

    User addUser(User user);

    void updateUser(User user);

    void deleteUserById(Integer userId);

    User findUserByLogin(String login);
}
