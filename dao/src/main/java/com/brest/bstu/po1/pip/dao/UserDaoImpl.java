package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class UserDaoImpl implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String ID = "id";

    private static final String SURNAME = "surname";

    private static final String NAME = "name";

    private static final String LOGIN = "login";

    private static final String PASSWORD = "password";

    private static final String EMAIL = "email";

    private static final String IS_ADMIN = "is_admin";

    @Value("${user.select}")
    private String GET_ALL_USERS_SQL;

    @Value("${user.selectById}")
    private String GET_USER_BY_ID_SQL;

    @Value("${user.insert}")
    private String ADD_USER_SQL;

    @Value("${user.update}")
    private String UPDATE_USER_SQL;

    @Value("${user.delete}")
    private String DELETE_USER_SQL;

    @Value("${user.findByLogin}")
    private String FIND_USER_BY_LOGIN_SQL;

    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Collection<User> getAllUsers() {
        LOGGER.debug("getAllUsers()");
        Collection<User> users = namedParameterJdbcTemplate.
                query(GET_ALL_USERS_SQL, new UserRowMapper());
        return users;
    }

    @Override
    public User getUserById(Integer userId) {
        LOGGER.debug("getUserById({})", userId);
        SqlParameterSource namedParameters = new MapSqlParameterSource(ID, userId);
        User user = namedParameterJdbcTemplate.queryForObject(
                GET_USER_BY_ID_SQL, namedParameters, new UserRowMapper()
        );
        return user;
    }

    @Override
    public User addUser(User user) {
        LOGGER.debug("addUser({})", user);
        MapSqlParameterSource namedParameters =
                new MapSqlParameterSource(SURNAME, user.getSurname());
        namedParameters.addValue(NAME, user.getName());
        namedParameters.addValue(LOGIN, user.getLogin());
        namedParameters.addValue(PASSWORD, user.getPassword());
        namedParameters.addValue(EMAIL, user.getEmail());
        namedParameters.addValue(IS_ADMIN, user.getIsAdmin());

        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(ADD_USER_SQL, namedParameters,
                generatedKeyHolder, new String[] {"id"});
        user.setId(generatedKeyHolder.getKey().intValue());
        return user;
    }

    @Override
    public void updateUser(User user) {
        LOGGER.debug("updateUser({})", user);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue(ID, user.getId());
        namedParameters.addValue(SURNAME, user.getName());
        namedParameters.addValue(NAME, user.getName());
        namedParameters.addValue(LOGIN, user.getLogin());
        namedParameters.addValue(PASSWORD, user.getPassword());
        namedParameters.addValue(EMAIL, user.getEmail());
        namedParameters.addValue(IS_ADMIN, user.getIsAdmin());

        namedParameterJdbcTemplate.update(UPDATE_USER_SQL, namedParameters);
    }

    @Override
    public void deleteUserById(Integer userId) {
        LOGGER.debug("deleteUserById({})", userId);
        SqlParameterSource namedParameters = new MapSqlParameterSource(ID, userId);
        namedParameterJdbcTemplate.update(DELETE_USER_SQL, namedParameters);
    }

    @Override
    public User findUserByLogin(String login) {
        try {
            LOGGER.debug("findUserByLogin({})", login);
            SqlParameterSource namedParameters = new MapSqlParameterSource(LOGIN, login);
            User user = namedParameterJdbcTemplate.queryForObject(
                    FIND_USER_BY_LOGIN_SQL, namedParameters, new UserRowMapper()
            );
            return user;
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setId(resultSet.getInt(ID));
            user.setSurname(resultSet.getString(SURNAME));
            user.setName(resultSet.getString(NAME));
            user.setLogin(resultSet.getString(LOGIN));
            user.setPassword(resultSet.getString(PASSWORD));
            user.setEmail(resultSet.getString(EMAIL));
            user.setIsAdmin(resultSet.getBoolean(IS_ADMIN));
            return user;
        }
    }
}
