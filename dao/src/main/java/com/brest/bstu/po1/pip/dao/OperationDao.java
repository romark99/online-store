package com.brest.bstu.po1.pip.dao;

import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;

import java.math.BigDecimal;
import java.util.Collection;

public interface OperationDao {

    boolean checkIfAlreadyAdded(Operation operation);

    void addOperation(Operation operation);

    void changeCartQuantity(Operation operation, Boolean isIncrease);

    Operation selectExistingOperation(Integer userId, Integer goodsId, Boolean isHistory);

    Collection<CartDTO> getCartDTOs(Integer userId, Boolean isHistory);

    BigDecimal getCartTotal(Integer userId, Boolean isHistory);

    void deleteOperation(Operation oldOperation);

    void pay(Integer userId);
}