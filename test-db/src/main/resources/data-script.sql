INSERT INTO goods (id, name, quantity, image_name, image, description, price, adding_date) VALUES (1, 'Шоколад', 5, null, null, null, 50.00, '2017-06-06');
INSERT INTO goods (id, name, quantity, image_name, image, description, price, adding_date) VALUES (2, 'Конфеты', 10, null, null, null, 20.00, '2016-05-04');
INSERT INTO goods (id, name, quantity, image_name, image, description, price, adding_date) VALUES (3, 'Цветы', 3, null, null, null, 50.00, '2017-06-06');
INSERT INTO goods (id, name, quantity, image_name, image, description, price, adding_date) VALUES (4, 'Чай', 7, null, null, null, 20.99, '2013-05-04');

INSERT INTO users (id, surname, name, login, password, email, is_admin) VALUES (2, 'Иванов', 'Иван', 'ivan', 'ivan', 'romark999@gmail.com', false);
INSERT INTO users (id, surname, name, login, password, email, is_admin) VALUES (1, 'Марковский', 'Роман', 'romark', 'romark', 'romark@tut.by', true);
