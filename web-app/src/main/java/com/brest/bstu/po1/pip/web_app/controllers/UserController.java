package com.brest.bstu.po1.pip.web_app.controllers;

import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.service.SecurityService;
import com.brest.bstu.po1.pip.service.UserService;
import com.brest.bstu.po1.pip.service.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger();

    private final UserService userService;

    private final SecurityService securityService;

    private final UserValidator userValidator;

    @Autowired
    public UserController(UserService userService, SecurityService securityService, UserValidator userValidator) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
    }

    @GetMapping(value = "/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("isRegistration", true);
        LOGGER.debug("registration({})", model);
        return "registration";
    }

    @GetMapping(value = "/profile")
    public String profile(Principal principal, Model model) {
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        model.addAttribute("userForm", user);
        model.addAttribute("isRegistration", false);
        LOGGER.debug("profile({})", model);
        return "registration";
    }

    @PostMapping(value = "/registration")
    public String registration(@ModelAttribute("userForm") @Valid User user, BindingResult bindingResult,
                               Model model) {
        userValidator.validate(user, bindingResult);
        LOGGER.debug("registration({}, {}, {})", user, bindingResult, model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isRegistration", true);
            return "registration";
        }
        else if (userService.findUserByLogin(user.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.user", "An account already exists for this login.");
            model.addAttribute("isRegistration", true);
            return "registration";
        }
        else if (!user.getPassword().equals(user.getConfirmedPassword())) {
            bindingResult.rejectValue("confirmedPassword", "error.user", "Passwords do not match.");
            model.addAttribute("isRegistration", true);
            return "registration";
        }
        userService.addUser(user);

        securityService.autoLogin(user.getLogin(), user.getConfirmedPassword());
//        model.addAttribute("userForm", new User());
        return "redirect:/goods";
    }

    @PostMapping(value = "/profile")
    public String profile(@ModelAttribute("userForm") @Valid User user, BindingResult bindingResult,
                               Model model) {
        userValidator.validate(user, bindingResult);
        LOGGER.debug("registration({}, {}, {})", user, bindingResult, model);
        if (bindingResult.hasErrors()) {
            model.addAttribute("isRegistration", false);
            return "registration";
        }
        userService.updateUser(user);
        securityService.autoLogin(user.getLogin(), user.getConfirmedPassword());
//        model.addAttribute("userForm", user);
        return "redirect:/goods";
    }

    @GetMapping(value = "/login")
    public String login(Model model, String error, String logout) {
        LOGGER.debug("login({}, {}, {})", model, error, logout);
        if (error != null) {

            model.addAttribute("error", "Username or password is incorrect.");
        }
        if (logout != null ) {
            model.addAttribute("message", "Logged out successfully.");
        }
        return "login";
    }
}
