package com.brest.bstu.po1.pip.web_app.controllers;

import com.brest.bstu.po1.pip.model.*;
import com.brest.bstu.po1.pip.service.GoodsService;
import com.brest.bstu.po1.pip.service.OperationService;
import com.brest.bstu.po1.pip.service.UserService;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.Collection;

@Controller
public class GoodsController {

    private static final Logger LOGGER = LogManager.getLogger();

    private final GoodsService goodsService;

    private final UserService userService;

    private final OperationService operationService;

    @Autowired
    public GoodsController(GoodsService goodsService, UserService userService, OperationService operationService) {
        this.goodsService = goodsService;
        this.userService = userService;
        this.operationService = operationService;
    }

    @GetMapping(value = {"/", "/goods"})
    public String goods(Model model) {
        Collection<Goods> threeGoods = goodsService.getThreeGoods();
        Collection<Goods> goodsList = goodsService.getAllGoods();
        String substring = "";
        model.addAttribute("threeGoods", threeGoods);
        model.addAttribute("goodsList", goodsList);
        model.addAttribute("substring", substring);
        LOGGER.debug("goods({})", model);
        return "goods";
    }

    @GetMapping(value = "/goods/search")
    public String searchGoods(@RequestParam(value = "substring", required = false) String substring, Model model) {
        Collection<Goods> goodsList;
        if (substring.trim().length() > 0) {
            goodsList = goodsService.searchGoods(substring);
        } else {
            goodsList = goodsService.getAllGoods();
        }
        model.addAttribute("goodsList", goodsList);
        LOGGER.debug("searchGoods({}, {})", substring, model);
        return "goods";
    }

    @GetMapping(value = "/good")
    public String gotoAddGoods(Model model) {
        Goods goods = new Goods();
        model.addAttribute("isAdding", true);
        model.addAttribute("good", goods);
        LOGGER.debug("gotoAddGoods({})", model);
        return "add_good";
    }

    @PostMapping(value = "/good")
    public String addGoods(@ModelAttribute("good") @Valid Goods goods,
                           @RequestParam("imgFile") MultipartFile multipartFile,
                           BindingResult result, Model model) {
        LOGGER.debug("addGoods({}, {})", goods, multipartFile, result, model);
        if (result.hasErrors()) {
            model.addAttribute("isAdding", true);
            return "add_good";
        } else {
            //TODO: To find another solution.
            String extenstion = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            goods.setImageExtension(extenstion);
            String data = UtilBase64Image.encoder(multipartFile);
            goods.setImage(data);
            this.goodsService.addGoods(goods);
            return "redirect:/goods";
        }
    }

    @GetMapping(value = "/good/edit/{id}")
    public String gotoEditGoods(@PathVariable Integer id, Model model) {
        Goods goods = goodsService.getGoodsById(id);
        model.addAttribute("isAdding", false);
        model.addAttribute("good", goods);
        LOGGER.debug("gotoEditGoods({}, {})", id, model);
        return "add_good";
    }

    @PostMapping(value = "/good/edit/{id}")
    public String editGoods(@ModelAttribute("good") @Valid Goods goods,
                            @RequestParam("imgFile") MultipartFile multipartFile,
                            BindingResult result, Model model) {
        LOGGER.debug("editGoods({}, {}, {}, {})", goods, result, model);
        if (result.hasErrors()) {
            model.addAttribute("isAdding", false);
            return "add_good";
        } else {
            String extenstion = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            goods.setImageExtension(extenstion);
            String data = UtilBase64Image.encoder(multipartFile);
            goods.setImage(data);
            this.goodsService.updateGoods(goods);
            return "redirect:/goods";
        }
    }

    @GetMapping(value = "/good/view/{id}")
    public String gotoViewGoods(@PathVariable Integer id, Principal principal, Model model) {
        Goods goods = goodsService.getGoodsById(id);
        Operation operation = new Operation();
        operation.setGoodsId(id);
        operation.setIsHistory(false);
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        operation.setUserId(user.getId());
        Operation existingOperation = operationService.getOperationIfExists(operation);
        if (existingOperation != null) {
            model.addAttribute("isAlreadyInCart", true);
            operation = existingOperation;
        } else {
            model.addAttribute("isAlreadyInCart", false);
            operation.setQuantity(0);
        }
        LOGGER.debug("gotoViewGoods({}, {}, {}, {})", id, model, operation, goods);
        model.addAttribute("goods", goods);
        model.addAttribute("operation", operation);
        return "view_good";
    }

    @GetMapping(value = "/cart")
    public String cart(Principal principal, Model model) {
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        Collection<CartDTO> cartDTOs = operationService.getCartDTOs(user.getId(), false);
        BigDecimal total = operationService.getCartTotal(user.getId(), false);
        model.addAttribute("carts", cartDTOs);
        model.addAttribute("cartSize", cartDTOs.size());
        model.addAttribute("total", total);
        LOGGER.debug("cart({})", model);
        return "cart";
    }

    @PostMapping(value = "/cart/update")
    public String addOrUpdateCart(@ModelAttribute("operation") Operation operation,
                                  BindingResult bindingResult,
                                  Model model) {

        LOGGER.debug("updateCart({}, {}, {})", operation, bindingResult, model);
        if (bindingResult.hasErrors()) {
            return "view_good";
        } else {
            this.operationService.updateCart(operation);
            return "redirect:/goods";
        }
    }

    @GetMapping(value = "/cart/{id}/delete")
    public String deleteFromCart(@PathVariable Integer id, Principal principal, Model model) {
        LOGGER.debug("deleteFromCart({}, {}, {})", id, principal, model);
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        this.operationService.deleteFromCart(user.getId(), id);
        return "redirect:/cart";
    }

    @PostMapping(value = "/pay")
    public String pay(Principal principal, Model model) {
        LOGGER.debug("pay({}, {})", principal, model);
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        this.operationService.pay(user.getId());
        return "redirect:/history";
    }

    @GetMapping(value = "/history")
    public String history(Principal principal, Model model) {
        String login = principal.getName();
        User user = userService.findUserByLogin(login);
        Collection<CartDTO> cartDTOs = operationService.getCartDTOs(user.getId(), true);
        BigDecimal total = operationService.getCartTotal(user.getId(), true);
        model.addAttribute("carts", cartDTOs);
        model.addAttribute("cartSize", cartDTOs.size());
        model.addAttribute("total", total);
        LOGGER.debug("history({})", model);
        return "history";
    }
}
