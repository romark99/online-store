DROP TABLE IF EXISTS goods;
create table goods
(
  id          integer           not null auto_increment
    constraint goods_pk
      primary key,
  name        varchar(100)      not null,
  quantity    integer default 0 not null,
  image_name  varchar(100),
  image       bytea,
  description varchar(500),
  price       numeric(10, 2)    not null,
  adding_date timestamp         not null
);

DROP TABLE IF EXISTS users;
create table users
(
  id       int                   not null auto_increment
    constraint users_pk
      primary key,
  surname  varchar(100)          not null,
  name     varchar(100)          not null,
  login    varchar(100)          not null,
  password varchar(100)          not null,
  email    varchar(100),
  is_admin boolean default false not null
);