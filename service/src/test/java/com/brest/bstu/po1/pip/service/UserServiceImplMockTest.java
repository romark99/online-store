package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.dao.UserDao;
import com.brest.bstu.po1.pip.model.User;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:service-mock-test.xml"})
public class UserServiceImplMockTest {
    private static final User PETROV =
            new User("Петров", "Петр", "petr", "petr", null, false);

    private static final User PAVLOV =
            new User("Павлов", "Иван", "pavel", "pavel", null, false);

    private static final List<User> USERS = Arrays.asList(PETROV, PAVLOV);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao mockUserDao;

    @After
    public void verifyAndReset() {
        EasyMock.verify(mockUserDao);
        EasyMock.reset(mockUserDao);
    }

    @Test
    public void getUsers() {
        EasyMock.expect(mockUserDao.getAllUsers()).andReturn(USERS);
        EasyMock.replay(mockUserDao);
        Collection<User> users = userService.getAllUsers();
        Assert.assertEquals(users, USERS);
    }

    @Test
    public void getUserById() {
        EasyMock.expect(mockUserDao.getUserById(EasyMock.anyInt())).andReturn(PETROV);
        EasyMock.replay(mockUserDao);
        User user = userService.getUserById(1);
        Assert.assertEquals(user, PETROV);
    }

//    @Test
//    public void addUser() {
//        EasyMock.expect(mockUserDao.addUser(EasyMock.anyObject(User.class)))
//                .andReturn(PETROV);
//        EasyMock.replay(mockUserDao);
//        User publication = userService.addUser(PETROV);
//        Assert.assertEquals(publication, PETROV);
//    }
//
//    @Test
//    public void updateUser() {
//        mockUserDao.updateUser(EasyMock.anyObject(User.class));
//        EasyMock.expectLastCall();
//        EasyMock.replay(mockUserDao);
//        userService.updateUser(PETROV);
//    }

    @Test
    public void deleteUserById() {
        mockUserDao.deleteUserById(EasyMock.anyInt());
        EasyMock.expectLastCall();
        EasyMock.replay(mockUserDao);
        userService.deleteUserById(1);
    }
}
