package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.dao.GoodsDao;
import com.brest.bstu.po1.pip.model.Goods;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:service-mock-test.xml"})
public class GoodsServiceImplMockTest {

    private static final Goods ICE_CREAM =
            new Goods("Мороженое", 2, null, null, "Очень вкусное мороженое",  new BigDecimal(60.40), Date.valueOf("2016-07-18"));

    private static final Goods COOKIE =
            new Goods("Печенье", 7, null, null, "Обычное печенье.",  new BigDecimal(7.15), Date.valueOf("2015-12-05"));

    private static final List<Goods> GOODS_LIST = Arrays.asList(ICE_CREAM, COOKIE);

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsDao mockGoodsDao;

    @After
    public void verifyAndReset() {
        EasyMock.verify(mockGoodsDao);
        EasyMock.reset(mockGoodsDao);
    }

    @Test
    public void getAllGoods() {
        EasyMock.expect(mockGoodsDao.getAllGoods()).andReturn(GOODS_LIST);
        EasyMock.replay(mockGoodsDao);
        Collection<Goods> allGoods = goodsService.getAllGoods();
        Assert.assertEquals(allGoods, GOODS_LIST);
    }

    @Test
    public void getThreeGoods() {
        EasyMock.expect(mockGoodsDao.getThreeGoods()).andReturn(GOODS_LIST);
        EasyMock.replay(mockGoodsDao);
        Collection<Goods> allGoods = goodsService.getThreeGoods();
        Assert.assertEquals(allGoods, GOODS_LIST);
    }

    @Test
    public void getGoodsById() {
        EasyMock.expect(mockGoodsDao.getGoodsById(EasyMock.anyInt())).andReturn(ICE_CREAM);
        EasyMock.replay(mockGoodsDao);
        Goods goods = goodsService.getGoodsById(1);
        Assert.assertEquals(goods, ICE_CREAM);
    }

    @Test
    public void addGoods() throws IOException {
        EasyMock.expect(mockGoodsDao.addGoods(EasyMock.anyObject(Goods.class)))
                .andReturn(COOKIE);
        EasyMock.replay(mockGoodsDao);
        Goods goods = goodsService.addGoods(COOKIE);
        Assert.assertEquals(goods, COOKIE);
    }

    @Test
    public void updateGoods() {
        mockGoodsDao.updateGoods(EasyMock.anyObject(Goods.class));
        EasyMock.expectLastCall();
        EasyMock.replay(mockGoodsDao);
        goodsService.updateGoods(ICE_CREAM);
    }

    @Test
    public void deleteGoodsById() {
        mockGoodsDao.deleteGoodsById(EasyMock.anyInt());
        EasyMock.expectLastCall();
        EasyMock.replay(mockGoodsDao);
        goodsService.deleteGoodsById(1);
    }

    @Test
    public void searchGoods() {
        EasyMock.expect(mockGoodsDao.searchGoods(EasyMock.anyString())).andReturn(GOODS_LIST);
        EasyMock.replay(mockGoodsDao);
        Collection<Goods> allGoods = goodsService.searchGoods("String");
        Assert.assertEquals(allGoods, GOODS_LIST);
    }
}