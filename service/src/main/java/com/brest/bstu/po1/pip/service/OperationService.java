package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;

@Service
public interface OperationService {

    void updateCart(Operation operation);

    Operation getOperationIfExists(Operation operation);

    Collection<CartDTO> getCartDTOs(Integer userId, Boolean isHistory);

    BigDecimal getCartTotal(Integer userId, Boolean isHistory);

    void deleteFromCart(Integer userId, Integer goodsId);

    void pay(Integer userId);
}
