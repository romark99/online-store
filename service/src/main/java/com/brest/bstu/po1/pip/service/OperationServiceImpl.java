package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.dao.GoodsDao;
import com.brest.bstu.po1.pip.dao.OperationDao;
import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;

public class OperationServiceImpl implements OperationService {

    private static final Logger LOGGER = LogManager.getLogger();

    private OperationDao operationDao;

    private GoodsDao goodsDao;

    @Autowired
    public OperationServiceImpl(OperationDao operationDao, GoodsDao goodsDao) {
        this.operationDao = operationDao;
        this.goodsDao = goodsDao;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCart(Operation operation) {
        LOGGER.debug("updateCart({})", operation);
        if (operationDao.checkIfAlreadyAdded(operation)) {
            Operation oldOperation = operationDao.selectExistingOperation(
                    operation.getUserId(),
                    operation.getGoodsId(),
                    operation.getIsHistory());
            operationDao.changeCartQuantity(oldOperation, false);
            goodsDao.changeQuantity(oldOperation, true);
            if (operation.getQuantity() != 0) {
                goodsDao.changeQuantity(operation, false);
                operationDao.changeCartQuantity(operation, true);
            } else {
                operationDao.deleteOperation(operation);
            }
        } else {
            goodsDao.changeQuantity(operation, false);
            operationDao.addOperation(operation);
        }
    }

    @Override
    @Transactional
    public Operation getOperationIfExists(Operation operation) {
        LOGGER.debug("getOperationIdIfExists({})", operation);
        Boolean isInCart = operationDao.checkIfAlreadyAdded(operation);
        if (isInCart) {
            Operation operationWithId =
                    operationDao.selectExistingOperation(operation.getUserId(),
                            operation.getGoodsId(), operation.getIsHistory());
            return operationWithId;
        } else {
            return null;
        }
    }

//    @Override
//    public Operation getExistingOperation(Operation operation) {
//        LOGGER.debug("getExistingOperation({})", operation);
//        Operation existingOperation = operationDao.selectExistingOperation(
//                operation.getUserId(),
//                operation.getGoodsId(),
//                operation.getIsHistory());
//        return existingOperation;
//    }

    @Override
    public Collection<CartDTO> getCartDTOs(Integer userId, Boolean isHistory) {
        LOGGER.debug("getCartDTOs({}), {})", userId, isHistory);
        Collection<CartDTO> cartDTOs = operationDao.getCartDTOs(userId, isHistory);
        return cartDTOs;
    }

    @Override
    public BigDecimal getCartTotal(Integer userId, Boolean isHistory) {
        LOGGER.debug("getCartTotal({}), {})", userId, isHistory);
        BigDecimal total = operationDao.getCartTotal(userId, isHistory);
        return total;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFromCart(Integer userId, Integer goodsId) {
        LOGGER.debug("deleteFromCart({}, {})", userId, goodsId);
        Operation oldOperation = operationDao.selectExistingOperation(userId, goodsId, false);
        goodsDao.changeQuantity(oldOperation, true);
        operationDao.deleteOperation(oldOperation);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pay(Integer userId) {
        LOGGER.debug("pay({})", userId);
        operationDao.pay(userId);
    }
}
