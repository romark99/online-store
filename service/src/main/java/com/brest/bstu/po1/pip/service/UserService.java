package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.model.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface UserService {

    Collection<User> getAllUsers();

    User getUserById(Integer userId);

    User addUser(User user);

    void updateUser(User user);

    void deleteUserById(Integer userId);

    User findUserByLogin(String login);
}