package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.dao.UserDao;
import com.brest.bstu.po1.pip.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;

public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LogManager.getLogger();

    private BCryptPasswordEncoder encoder;

    private UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder encoder) {
        this.userDao = userDao;
        this.encoder = encoder;
    }

    @Override
    public Collection<User> getAllUsers() {
        LOGGER.debug("getUsers()");
        Collection<User> users = userDao.getAllUsers();
        return users;
    }

    @Override
    public User getUserById(Integer userId) {
        LOGGER.debug("getUserById({})", userId);
        User user = userDao.getUserById(userId);
        return user;
    }

    @Override
    public User addUser(User user) {
        LOGGER.debug("addUser({})", user);
        user.setPassword(encoder.encode(user.getPassword()));
        User addedUser = userDao.addUser(user);
        return addedUser;
    }

    @Override
    public void updateUser(User user) {
        LOGGER.debug("updateUser({})", user);
        user.setPassword(encoder.encode(user.getPassword()));
        userDao.updateUser(user);
    }

    @Override
    public void deleteUserById(Integer userId) {
        LOGGER.debug("deleteUserById({})", userId);
        userDao.deleteUserById(userId);
    }

    @Override
    public User findUserByLogin(String login) {
        LOGGER.debug("findUserByLogin({})", login);
        User user = userDao.findUserByLogin(login);
        return user;
    }
}
