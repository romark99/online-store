package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.dao.GoodsDao;
import com.brest.bstu.po1.pip.model.Goods;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;

public class GoodsServiceImpl implements GoodsService {
    private static final Logger LOGGER = LogManager.getLogger();

    private GoodsDao goodsDao;

    public GoodsServiceImpl(GoodsDao goodsDao) {
        this.goodsDao = goodsDao;
    }

    @Override
    public Collection<Goods> getAllGoods() {
        LOGGER.debug("getAllGoods()");
        Collection<Goods> goodsList =
                goodsDao.getAllGoods();
        return goodsList;
    }

    @Override
    public Collection<Goods> getThreeGoods() {
        LOGGER.debug("getThreeGoods()");
        Collection<Goods> goodsList =
                goodsDao.getThreeGoods();
        return goodsList;
    }

    @Override
    public Goods getGoodsById(Integer goodsId) {
        LOGGER.debug("getGoodsById({})", goodsId);
        Goods goods =
                goodsDao.getGoodsById(goodsId);
        return goods;
    }

    @Override
    public Goods addGoods(Goods goods) {
        LOGGER.debug("addGoods({})", goods);
        Goods addedGoods =
                goodsDao.addGoods(goods);
        return addedGoods;
    }

    @Override
    public void updateGoods(Goods goods) {
        LOGGER.debug("updateGoods({})", goods);
        goodsDao.updateGoods(goods);

    }

    @Override
    public void deleteGoodsById(Integer goodsId) {
        LOGGER.debug("deleteGoodsById({})", goodsId);
        goodsDao.deleteGoodsById(goodsId);
    }

    @Override
    public Collection<Goods> searchGoods(String substring) {
        LOGGER.debug("searchGoods({})", substring);
        Collection<Goods> goodsList;
        goodsList = goodsDao.searchGoods(substring);
        return goodsList;
    }
}
