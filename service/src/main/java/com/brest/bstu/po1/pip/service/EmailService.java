package com.brest.bstu.po1.pip.service;

import org.springframework.stereotype.Service;

@Service
public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text, String path, String extension);
}
