package com.brest.bstu.po1.pip.service;

import org.springframework.stereotype.Service;

@Service
public interface SecurityService {

    void autoLogin(String login, String password);
}
