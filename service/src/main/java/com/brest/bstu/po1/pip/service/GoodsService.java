package com.brest.bstu.po1.pip.service;

import com.brest.bstu.po1.pip.model.Goods;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface GoodsService {

    Collection<Goods> getAllGoods();

    Collection<Goods> getThreeGoods();

    Goods getGoodsById(Integer goodsId);

    Goods addGoods(Goods goods);

    void updateGoods(Goods goods);

    void deleteGoodsById(Integer publicationId);

    Collection<Goods> searchGoods(String substring);
}