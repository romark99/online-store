package com.brest.bstu.po1.pip.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = LogManager.getLogger();

    private final JavaMailSender emailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void sendSimpleMessage(
            String to, String subject, String text, String path, String extension) {
        LOGGER.debug("sendSimpleMessage({}, {}, {}, {}, {})", to, subject, text, path, extension);
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource file = new FileSystemResource(new File(path));
            helper.addAttachment("Image." + extension, file);
            emailSender.send(message);
        } catch (MessagingException e) {
            LOGGER.debug("MessagingException!!!!");
            e.printStackTrace();
        }

    }
}
