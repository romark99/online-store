package com.brest.bstu.po1.pip.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class SecurityServiceImpl implements SecurityService {

    private static final Logger LOGGER = LogManager.getLogger();

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    @Autowired
    public SecurityServiceImpl(AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    //HI
//    @Override
//    public String findLoggedInLogin() {
//        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
////        String name = SecurityContextHolder.getContext().getAuthentication().getName();
//        LOGGER.debug("findLoggedInLogin({})", userDetails);
////        LOGGER.debug("autoLogin({}, {})", userDetails, name);
//        if (userDetails instanceof UserDetails) {
//            return ((UserDetails) userDetails).getUsername();
//        }
//        return null;
//    }

    @Override
    public void autoLogin(String login, String password) {
        LOGGER.debug("autoLogin({}, {})", login, password);
        UserDetails userDetails = userDetailsService.loadUserByUsername(login);
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        authenticationManager.authenticate(authenticationToken);
        if (authenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            LOGGER.debug("Successfully {} logged in.", login);
        }
    }
}
