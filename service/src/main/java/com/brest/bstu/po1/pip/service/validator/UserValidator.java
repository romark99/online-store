package com.brest.bstu.po1.pip.service.validator;

//import com.brest.bstu.po1.pip.model.User;
//import com.brest.bstu.po1.pip.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

//import org.springframework.validation.ValidationUtils;

public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {

    }

//    private UserService userService;
//
//    @Autowired
//    public UserValidator(UserService userService) {
//        this.userService = userService;
//    }
//
//    @Override
//    public boolean supports(Class<?> aClass) {
//        return User.class.equals(aClass);
//    }
//
//    @Override
//    public void validate(Object o, Errors errors) {
//        User user = (User) o;
//
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "Required");
//        if (user.getLogin().length() < 2 || user.getLogin().length() > 32) {
//            errors.rejectValue("login", "Size.userForm.login");
//        }
//        if (userService.findUserByLogin(user.getLogin()) != null) {
//            errors.rejectValue("login", "Duplicate.userForm.login");
//        }
//
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required");
//        if (user.getPassword().length() <= 3) {
//            errors.rejectValue("password", "Size.userForm.password");
//        }
//        if (!user.getConfirmedPassword().equals(user.getPassword())) {
//            errors.rejectValue("confirmedPassword", "Different.userForm.password");
//        }
//    }
}
