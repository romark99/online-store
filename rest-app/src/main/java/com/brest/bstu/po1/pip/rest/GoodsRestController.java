package com.brest.bstu.po1.pip.rest;

import com.brest.bstu.po1.pip.model.Goods;
import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.model.UtilBase64Image;
import com.brest.bstu.po1.pip.service.EmailService;
import com.brest.bstu.po1.pip.service.GoodsService;
import com.brest.bstu.po1.pip.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class GoodsRestController {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private GoodsService goodsService;

    private UserService userService;

    private EmailService emailService;

    @Autowired
    public GoodsRestController(GoodsService goodsService, UserService userService, EmailService emailService) {
        this.goodsService = goodsService;
        this.userService = userService;
        this.emailService = emailService;
    }

    //curl -X GET -v http://localhost:8086/goods
    @GetMapping(value = "/goods")
    @ResponseStatus(HttpStatus.OK)
    public Collection<Goods> getAllGoods() {
        LOGGER.debug("getAllGoods()");
        Collection<Goods> goods = goodsService.getAllGoods();
        for (Goods good: goods) {
            String path = "./images/" + good.getId();
            String imageBase64 = UtilBase64Image.encoder(path, good.getImageExtension());
            good.setImage(imageBase64);
        }
        return goods;
    }

    //curl -X GET -v http://localhost:8086/goods/three
    @GetMapping(value = "/goods/three")
    @ResponseStatus(HttpStatus.OK)
    public Collection<Goods> getThreeGoods() {
        LOGGER.debug("getThreeGoods()");
        Collection<Goods> goods = goodsService.getThreeGoods();
        for (Goods good: goods) {
            String path = "./images/" + good.getId();
            String imageBase64 = UtilBase64Image.encoder(path, good.getImageExtension());
            good.setImage(imageBase64);
        }
        return goods;
    }

    //curl -X GET -v http://localhost:8086/goods/get/4
    @GetMapping(value = "/goods/get/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public Goods getGoodsById(
            @PathVariable(value = "id") Integer id) {
        LOGGER.debug("getGoodsById({})", id);
        Goods good = goodsService.getGoodsById(id);
        String path = "./images/" + good.getId();
        String imageBase64 = UtilBase64Image.encoder(path, good.getImageExtension());
        good.setImage(imageBase64);
        return good;
    }

    //curl -H "Content-Type: application/json" -X POST -d '{"name":"Computer","quantity":"50","description":"A powerful computer","price":"70.56"}' -v http://localhost:8086/goods
    @PostMapping(value = "/goods")
    @ResponseStatus(HttpStatus.CREATED)
    public Goods addGoods(@RequestBody Goods goods) {
        LOGGER.debug("addGoods({})", goods);
        String img = goods.getImage();
        goods.setImage(null);
        Goods newGoods =
                goodsService.addGoods(goods);
        String path = "./images/" + newGoods.getId();
        UtilBase64Image.decoder(img, path);
        Collection<User> customers = userService.getAllUsers();
        for (User customer: customers) {
            if (!customer.getIsAdmin()) {
                emailService.sendSimpleMessage(customer.getEmail(),
                        "New goods: " + newGoods.getName(),
                        "Price: " + newGoods.getPrice() + "$\nDescription:\n" + newGoods.getDescription(),
                        path, goods.getImageExtension());
            }
        }
        return newGoods;
    }

    //curl -H "Content-Type: application/json" -X POST -d '{"id":"9","name":"Cat","quantity":"50","description":"A powerful cat","price":"70.56"}' -v http://localhost:8086/goods/9
    @PostMapping(value = "/goods/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateGoods(
            @RequestBody Goods goods,
            @PathVariable (value="id") Integer id) {
        LOGGER.debug("addGoods({})", goods);
        String path = "./images/" + id;
        UtilBase64Image.decoder(goods.getImage(), path);
        goods.setImage(null);
        goodsService.updateGoods(goods);
    }

    //curl -X DELETE -v http://localhost:8086/goods/9
    @DeleteMapping(value = "/goods/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public void deleteGoods(@PathVariable(value = "id") Integer id) {
        LOGGER.debug("deleteGoods({})", id);
        goodsService.deleteGoodsById(id);
    }

    //curl -X GET -v http://localhost:8086/goods/search/ты
    @GetMapping(value = "/goods/search/{substring}")
    @ResponseStatus(HttpStatus.FOUND)
    public Collection<Goods> searchGoods(@PathVariable(value = "substring") String substring) {
        LOGGER.debug("searchGoods({})", substring);
        Collection<Goods> goodsList = goodsService.searchGoods(substring);
        for (Goods good: goodsList) {
            String path = "./images/" + good.getId();
            String imageBase64 = UtilBase64Image.encoder(path, good.getImageExtension());
            good.setImage(imageBase64);
        }
        return goodsList;
    }
}
