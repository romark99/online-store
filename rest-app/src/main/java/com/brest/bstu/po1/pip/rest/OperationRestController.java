package com.brest.bstu.po1.pip.rest;

import com.brest.bstu.po1.pip.model.CartDTO;
import com.brest.bstu.po1.pip.model.Operation;
import com.brest.bstu.po1.pip.model.UtilBase64Image;
import com.brest.bstu.po1.pip.service.OperationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collection;

@RestController
public class OperationRestController {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private OperationService operationService;

    //curl -H "Content-Type: application/json" -X POST -d '{"userId":"15","goodsId":"2","quantity":"1","isHistory":"false"}' -v http://localhost:8086/operations/cart/update
    //curl -H "Content-Type: application/json" -X POST -d '{"userId":"15","goodsId":"7","quantity":"2","isHistory":"false"}' -v http://localhost:8086/operations/cart/update
    @PostMapping(value = "/operations/cart/update")
    @ResponseStatus(HttpStatus.OK)
    public void updateCart(@RequestBody Operation operation) {
        LOGGER.debug("updateCart({})", operation);
        operationService.updateCart(operation);
    }

    //curl -H "Content-Type: application/json" -X POST -d '{"userId":"15","goodsId":"2","isHistory":"false"}' -v http://localhost:8086/operations
    @PostMapping(value = "/operations")
    @ResponseStatus(HttpStatus.FOUND)
    public Operation getOperationIfExists(@RequestBody Operation operation) {
        LOGGER.debug("getOperationIfExists({})", operation);
        Operation existingOperation = operationService.getOperationIfExists(operation);
        return existingOperation;
    }

    //curl -X GET -v http://localhost:8086/operations/carts/get/15/true
    @GetMapping (value = "/operations/carts/get/{user_id}/{is_history}")
    @ResponseStatus(HttpStatus.OK)
    public Collection<CartDTO> getCartDTOs(@PathVariable(value = "user_id") Integer userId,
                                 @PathVariable(value = "is_history") Boolean isHistory) {
        LOGGER.debug("getCartDTOs({}), {})", userId, isHistory);
        Collection<CartDTO> cartDTOs = operationService.getCartDTOs(userId, isHistory);
        for (CartDTO cart: cartDTOs) {
            String path = "./images/" + cart.getGoodsId();
            String imageBase64 = UtilBase64Image.encoder(path, cart.getImageExtension());
            cart.setImage(imageBase64);
        }
        return cartDTOs;
    }

    //curl -X GET -v http://localhost:8086/operations/carts/total/15/false
    @GetMapping (value = "/operations/carts/total/{user_id}/{is_history}")
    @ResponseStatus(HttpStatus.OK)
    public BigDecimal getCartTotal(@PathVariable(value = "user_id") Integer userId,
                                  @PathVariable(value = "is_history") Boolean isHistory) {
        LOGGER.debug("getCartTotal({}), {})", userId, isHistory);
        BigDecimal total = operationService.getCartTotal(userId, isHistory);
        return total;
    }

    //curl -X DELETE -v http://localhost:8086/operations/carts/15/7
    @DeleteMapping (value = "/operations/carts/{user_id}/{goods_id}")
    @ResponseStatus(HttpStatus.FOUND)
    public void deleteFromCart(@PathVariable(value = "user_id") Integer userId,
                                   @PathVariable(value = "goods_id") Integer goodsId) {
        LOGGER.debug("deleteFromCart({}, {})", userId, goodsId);
        operationService.deleteFromCart(userId, goodsId);
    }

    //curl -X GET -v http://localhost:8086/operations/pay/15
    @GetMapping (value = "/operations/pay/{user_id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void pay(@PathVariable(value = "user_id") Integer userId) {
        LOGGER.debug("pay({})", userId);
        operationService.pay(userId);
    }
}
