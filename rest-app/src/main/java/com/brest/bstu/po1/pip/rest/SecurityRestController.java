package com.brest.bstu.po1.pip.rest;

import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.service.SecurityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityRestController {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private SecurityService securityService;

    @Autowired
    public SecurityRestController(SecurityService securityService) {
        this.securityService = securityService;
    }

//    //?????????????????????????????
//    @GetMapping(value = "/security/login")
//    @ResponseStatus(HttpStatus.FOUND)
//    public String findLoggedInLogin() {
//        LOGGER.debug("findLoggedInLogin()");
//        String login = securityService.findLoggedInLogin();
//        return login;
//    }

    //
    @PostMapping(value = "/security/autologin")
    @ResponseStatus(HttpStatus.FOUND)
    public void autoLogin(@RequestBody User user) {
        LOGGER.debug("autoLogin({}), login");
        securityService.autoLogin(user.getLogin(), user.getConfirmedPassword());
    }
}
