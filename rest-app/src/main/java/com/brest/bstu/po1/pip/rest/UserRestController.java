package com.brest.bstu.po1.pip.rest;

import com.brest.bstu.po1.pip.model.User;
import com.brest.bstu.po1.pip.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class UserRestController {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    //curl -X GET -v http://localhost:8086/users
    @GetMapping(value = "/users")
    @ResponseStatus(HttpStatus.OK)
    public Collection<User> getAllUsers() {
        LOGGER.debug("getAllUsers()");
        Collection<User> users = userService.getAllUsers();
        return users;
    }

    //curl -X GET -v http://localhost:8086/get/4
    @GetMapping(value = "/users/get/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public User getUserById(@PathVariable(value = "id") Integer id) {
        LOGGER.debug("getUserById({})", id);
        User user = userService.getUserById(id);
        return user;
    }

    //curl -X GET -v http://localhost:8086/login/romark
    @GetMapping(value = "/users/login/{login}")
    @ResponseStatus(HttpStatus.FOUND)
    public User findUserByLogin(@PathVariable(value = "login") String login) {
        LOGGER.debug("findUserByLogin({})", login);
        User user = userService.findUserByLogin(login);
        return user;
    }

    //curl -H "Content-Type: application/json" -X POST -d '{"surname":"ivanov","name":"ivan","login":"ivan","password":"ivan","email":"ivan@yandex.ru","isAdmin":"false"}' -v http://localhost:8086/users
    @PostMapping(value = "/users")
    @ResponseStatus(HttpStatus.CREATED)
    public User addUser(@RequestBody User user) {
        LOGGER.debug("addUser({})", user);
        User newUser = userService.addUser(user);
        return newUser;
    }

    //curl -H "Content-Type: application/json" -X POST -d '{"id":"19","surname":"petrov","name":"petr","login":"ivan","password":"ivan","email":"ivan@mail.ru","isAdmin":"false"}' -v http://localhost:8086/users/19
    @PostMapping(value = "/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateUser (@RequestBody User user,
            @PathVariable (value="id") Integer id) {
        LOGGER.debug("updateUserAndLogin({}, {})", user, id);
        userService.updateUser(user);
    }

    //curl -X DELETE -v http://localhost:8086/users/19
    @DeleteMapping(value = "/users/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public void deleteUser(@PathVariable(value = "id") Integer id) {
        LOGGER.debug("deleteUser({})", id);
        userService.deleteUserById(id);
    }
}
